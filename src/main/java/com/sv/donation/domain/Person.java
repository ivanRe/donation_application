package com.sv.donation.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;


@Entity
@Data
@Table(name = "person")
public class Person implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "person_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView
    Integer personId;



    @Column(name = "person_name",length =255, nullable = false)
    @JsonView
    String personName;


    @Column(name = "person_surename",length =255, nullable = false)
    @JsonView
    String personSurename;


    @Column(name = "person_document_id",length =255, nullable = false)
    @JsonView
    String personDocumentId;


    @Column(name = "person_email",length =255, nullable = false)
    @JsonView
    String personEmail;


    @CreationTimestamp
    @JsonView
    private LocalDateTime createDateTime;


    @CreationTimestamp
    @JsonView
    private LocalDateTime updateDateTime;


    @ManyToOne(fetch = FetchType.LAZY)
    @Getter(onMethod = @__( @JsonIgnore))
    @JoinColumns({@JoinColumn(name = "department_department_id", referencedColumnName = "department_id")})
    Department department;

    @OneToMany(mappedBy = "person", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @Getter(onMethod = @__(@JsonIgnore))
    List<User> users;




}


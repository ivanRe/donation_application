package com.sv.donation.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Subselect;

import javax.persistence.Entity;
import javax.persistence.Id;

@Subselect("SELECT \t\n" +
        "\t\td.donation_id\n" +
        "\t\t,p.person_surename\n" +
        "\t\t,p.person_name\n" +
        "\t\t,p.person_email\n" +
        "\t\t,p.person_document_id\n" +
        "\t\t,c.country_name\n" +
        "\t\t,de.department_name\n" +
        "\t\t,i.institution_name\n" +
        "\t\t,CONCAT(\"$\",FORMAT(d.donation_amount,2)) AS donation_amount\n" +
        "\t\t,d.create_date_time\n" +
        "FROM \tdonation d\n" +
        "JOIN\tuser \t\tu\tON\td.user_user_id \t\t\t\t\t= u.user_id\n" +
        "JOIN \tperson \t\tp\tON\tu.person_person_id \t\t\t\t= p.person_id\n" +
        "JOIN\tinstitution i\tON\td.institution_institution_id \t= i.institution_id\n" +
        "JOIN\tdepartment\tde\tON \tp.department_department_id\t\t= de.department_id\n" +
        "JOIN \tcountry\t\tc \tON \tde.country_country_id\t\t\t= c.country_id")
@Entity
@Data
@NoArgsConstructor
public class DonationView {

    @Id
    Integer donationId;
    String person_surename;
    String person_name;
    String person_email;
    String person_document_id;
    String country_name;
    String department_name;
    String institution_name;
    String donation_amount;
    String create_date_time;
}

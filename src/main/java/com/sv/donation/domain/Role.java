package com.sv.donation.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.*;

import lombok.Data;

import java.util.List;


import org.hibernate.annotations.CreationTimestamp;
import lombok.Getter;


@Entity
@Data
@Table(name = "role")
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "role_id", nullable = false)
    @JsonView
    Integer roleId;



    @Column(name = "role_name",length =255, nullable = false)
    @JsonView
    String roleName;


    @CreationTimestamp
    @JsonView
    private LocalDateTime createDateTime;


    @CreationTimestamp
    @JsonView
    private LocalDateTime updateDateTime;


    @OneToMany(mappedBy = "role", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @Getter(onMethod = @__(@JsonIgnore))
    List<User> users;


}


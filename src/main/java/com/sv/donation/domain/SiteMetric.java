package com.sv.donation.domain;

import com.fasterxml.jackson.annotation.JsonView;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;


@Entity
@Data
@Table(name = "site_metric")
public class SiteMetric implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "site_metric_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView
    Integer siteMetricId;


    @Column(name = "site_metric_count", nullable = false)
    @JsonView
    Integer siteMetricCount;


    @CreationTimestamp
    @JsonView
    private LocalDateTime createDateTime;


    @CreationTimestamp
    @JsonView
    private LocalDateTime updateDateTime;



}


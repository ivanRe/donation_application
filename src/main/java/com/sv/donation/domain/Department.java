package com.sv.donation.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import java.io.Serializable;

import javax.persistence.*;


import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;


@Entity
@Data
@Table(name = "department")
public class Department implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "department_id", nullable = false)
        @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView
    Integer departmentId;


    @Column(name = "department_name", length = 255, nullable = false)
    @JsonView
    String departmentName;


    @CreationTimestamp
    @JsonView
    private LocalDateTime createDateTime;


    @CreationTimestamp
    @JsonView
    private LocalDateTime updateDateTime;


    @ManyToOne(fetch = FetchType.LAZY)
    @Getter(onMethod = @__(@JsonIgnore))
    @JoinColumns({@JoinColumn(name = "country_country_id", referencedColumnName = "country_id")})
    Country country;

    @OneToMany(mappedBy = "department", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @Getter(onMethod = @__(@JsonIgnore))
    List<Person> persons;


}


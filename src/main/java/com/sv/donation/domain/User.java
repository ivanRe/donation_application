package com.sv.donation.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;


@Entity
@Data
@Table(name = "user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "user_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView
    Integer userId;


    @Column(name = "user_name", length = 255, nullable = false)
    @JsonView
    String userName;


    @Column(name = "user_password", length = 255, nullable = false)
    @JsonView
    String userPassword;


    @CreationTimestamp
    @JsonView
    private LocalDateTime createDateTime;


    @CreationTimestamp
    @JsonView
    private LocalDateTime updateDateTime;


    @ManyToOne(fetch = FetchType.LAZY)
    @Getter(onMethod = @__(@JsonIgnore))
    @JoinColumns({@JoinColumn(name = "person_person_id", referencedColumnName = "person_id")})
    Person person;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @Getter(onMethod = @__(@JsonIgnore))
    List<Donation> donations;

    @ManyToOne(fetch = FetchType.EAGER)
    @Getter(onMethod = @__( @JsonIgnore))
    @JoinColumns({@JoinColumn(name = "role_role_id", referencedColumnName = "role_id")})
    Role role;

}


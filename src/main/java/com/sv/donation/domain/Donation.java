package com.sv.donation.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;


@Entity
@Data
@Table(name = "donation")
public class Donation implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "donation_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView
    Integer donationId;


    @Column(name = "donation_amount",scale = 4, precision = 13, nullable = false)
    @JsonView
    BigDecimal donationAmount;


    @CreationTimestamp
    @JsonView
    private LocalDateTime createDateTime;


    @CreationTimestamp
    @JsonView
    private LocalDateTime updateDateTime;


    @ManyToOne(fetch = FetchType.LAZY)
    @Getter(onMethod = @__( @JsonIgnore))
    @JoinColumns({@JoinColumn(name = "user_user_id", referencedColumnName = "user_id")})
    User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @Getter(onMethod = @__( @JsonIgnore))
    @JoinColumns({@JoinColumn(name = "institution_institution_id", referencedColumnName = "institution_id")})
    Institution institution;



}


package com.sv.donation.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.io.Serializable;

import javax.persistence.*;

import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;


@Entity
@Setter
@Getter
@Table(name = "country")
public class Country implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "country_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView
    Integer countryId;



    @Column(name = "country_name",length =255, nullable = false)
    @JsonView
    String countryName;


    @CreationTimestamp
    @JsonView
    private LocalDateTime createDateTime;


    @CreationTimestamp
    @JsonView
    private LocalDateTime updateDateTime;


    @OneToMany(mappedBy = "country", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @Getter(onMethod = @__(@JsonIgnore))
    List<Department> departments;

    @OneToMany(mappedBy = "country", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @Getter(onMethod = @__(@JsonIgnore))
    List<Institution> institutions;





}


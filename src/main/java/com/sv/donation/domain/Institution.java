package com.sv.donation.domain;


import com.fasterxml.jackson.annotation.JsonView;

import java.io.Serializable;
import javax.persistence.*;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.List;


@Entity
@Data
@Table(name = "institution")
public class Institution implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "institution_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView
    Integer institutionId;


    @Column(name = "institution_name", length = 255, nullable = false)
    @JsonView
    String institutionName;


    @CreationTimestamp
    @JsonView
    private LocalDateTime createDateTime;


    @CreationTimestamp
    @JsonView
    private LocalDateTime updateDateTime;


    @ManyToOne(fetch = FetchType.LAZY)

    @JoinColumns({@JoinColumn(name = "country_country_id", referencedColumnName = "country_id")})
    Country country;

    @OneToMany(mappedBy = "institution", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)

    List<Donation> donations;


}


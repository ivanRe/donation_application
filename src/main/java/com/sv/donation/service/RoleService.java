package com.sv.donation.service;

import com.sv.donation.domain.Role;
import com.sv.donation.repository.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    private RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role getById(Integer roleId){
        return this.roleRepository.getOne(roleId);
    }
}

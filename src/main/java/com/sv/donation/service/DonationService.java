package com.sv.donation.service;

import com.sv.donation.domain.Donation;
import com.sv.donation.domain.Institution;
import com.sv.donation.domain.User;
import com.sv.donation.dto.DonationFormDto;
import com.sv.donation.repository.DonationRepository;
import com.sv.donation.repository.InstitutionRepository;
import com.sv.donation.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class DonationService {

    private DonationRepository donationRepository;
    private UserService userService;
    private InstitutionService institutionService;

    public DonationService(DonationRepository donationRepository, UserService userService, InstitutionService institutionService) {
        this.donationRepository = donationRepository;
        this.userService = userService;
        this.institutionService = institutionService;
    }

    public void save (DonationFormDto donationFormDto, Integer userId){
        Donation donation = new Donation();
        Institution institution = this.institutionService.getById(donationFormDto.getInstitutionId());
        User user = this.userService.getById(userId);
        donation.setInstitution(institution);
        donation.setUser(user);
        donation.setDonationAmount(new BigDecimal(donationFormDto.getAmount()));
        this.donationRepository.saveAndFlush(donation);
    }

    public Optional<Donation> getLastDonation(Integer userId){
        return Optional.ofNullable(this.donationRepository.findLastDonationByUser(userId));
    }

}

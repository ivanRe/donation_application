package com.sv.donation.service;


import com.sv.donation.domain.Department;
import com.sv.donation.domain.Person;
import com.sv.donation.domain.Role;
import com.sv.donation.domain.User;
import com.sv.donation.dto.UserFormDto;
import com.sv.donation.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;
    private PersonService personService;
    private DepartmentService departmentService;
    private RoleService roleService;

    public UserService(UserRepository userRepository, PersonService personService, DepartmentService departmentService, RoleService roleService) {
        this.userRepository = userRepository;
        this.personService = personService;
        this.departmentService = departmentService;
        this.roleService = roleService;
    }

    public User guardarDTO(UserFormDto userFormDto){

        //TODO: VERIFICAR QUE NO EXISTA NI EMAIL NI USUARIO PREVIO A GUARDAR
        Department department = this.departmentService.getDepartmentById(userFormDto.getPersonDepartmentId());
        Person person = new Person();
        person.setPersonName(userFormDto.getPersonName());
        person.setPersonEmail(userFormDto.getPersonEmail());
        person.setPersonSurename(userFormDto.getPersonSureName());
        person.setDepartment(department);
        person.setPersonDocumentId(userFormDto.getPersonDocumentId());
        person = this.personService.save(person);

        Role role = this.roleService.getById(2); //ROLE USER

        User user = new User();
        user.setPerson(person);
        user.setUserName(userFormDto.getUserName());
        user.setUserPassword(new BCryptPasswordEncoder().encode(userFormDto.getUserPassword()));
        user.setRole(role);

        user = this.userRepository.saveAndFlush(user);

        return user;
    }

    public User getById(Integer userId){
        return this.userRepository.getOne(userId);
    }

    public User getByName(String userName){
        return this.userRepository.findByUserName(userName);
    }
}

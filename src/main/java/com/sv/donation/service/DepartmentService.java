package com.sv.donation.service;

import com.sv.donation.domain.Department;
import com.sv.donation.repository.DepartmentRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentService {

    private DepartmentRepository departmentRepository;

    private CountryService countryService;

    public DepartmentService(DepartmentRepository departmentRepository,CountryService countryService) {
        this.departmentRepository = departmentRepository;
        this.countryService = countryService;
    }

    public List<Department> getAllByCountry(Integer countryId){
        return  this.countryService.getById(countryId)
                .map(country -> this.departmentRepository.findByCountry(country))
                .orElse(new ArrayList<>());

    }

    public Department getDepartmentById(Integer departmentId){
        return this.departmentRepository.getOne(departmentId);
    }
}

package com.sv.donation.service;

import com.sv.donation.domain.SiteMetric;
import com.sv.donation.repository.SiteMetricRepository;
import org.springframework.stereotype.Service;

@Service
public class SiteMetricService {

    private SiteMetricRepository siteMetricRepository;

    public SiteMetricService(SiteMetricRepository siteMetricRepository) {
        this.siteMetricRepository = siteMetricRepository;
    }

    public Integer getTotalVisits(){
        return this.siteMetricRepository.getTotalVisits();
    }

    public SiteMetric saveDayMetrics(SiteMetric siteMetric){
        return this.siteMetricRepository.saveAndFlush(siteMetric);
    }

    public SiteMetric getLast(){return this.siteMetricRepository.getLastRow();}
}

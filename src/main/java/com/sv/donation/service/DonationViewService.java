package com.sv.donation.service;

import com.sv.donation.domain.DonationView;
import com.sv.donation.repository.DonationViewRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DonationViewService {

    private DonationViewRepository donationViewRepository;

    public DonationViewService(DonationViewRepository donationViewRepository) {
        this.donationViewRepository = donationViewRepository;
    }

    public List<DonationView> getAll(){
        return this.donationViewRepository.findAll();
    }

}

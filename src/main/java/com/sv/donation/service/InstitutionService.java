package com.sv.donation.service;

import com.sv.donation.domain.Institution;
import com.sv.donation.repository.InstitutionRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InstitutionService {

    private InstitutionRepository institutionRepository;
    private CountryService countryService;

    public InstitutionService(InstitutionRepository institutionRepository, CountryService countryService) {
        this.institutionRepository = institutionRepository;
        this.countryService = countryService;
    }

    public List<Institution> getAllByCountry(Integer countryId){
        return this.countryService.getById(countryId)
                .map(country -> this.institutionRepository.findAllByCountry(country))
                .orElse(new ArrayList<>());
    }

    public Institution getById(Integer institutionId){
        return this.institutionRepository.getOne(institutionId);
    }
}

package com.sv.donation.service;

import com.sv.donation.domain.Person;
import com.sv.donation.repository.PersonRepository;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    private PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person save(Person person){
        return this.personRepository.saveAndFlush(person);
    }
}

package com.sv.donation.service;

import com.sv.donation.domain.Country;
import com.sv.donation.repository.CountryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CountryService {

    private CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public List<Country> getAll(){
        return this.countryRepository.findAll();
    }

    public Optional<Country> getById(Integer countryId){
        return this.countryRepository.findById(countryId);

    }

    public Country getCountryById(Integer countryId){
        return this.countryRepository.getOne(countryId);
    }
}

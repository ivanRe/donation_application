package com.sv.donation.repository;

import com.sv.donation.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository <User,    Integer >,PagingAndSortingRepository<User,Integer > {

    User findByUserName(String username);
}


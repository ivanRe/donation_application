package com.sv.donation.repository;

import com.sv.donation.domain.SiteMetric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SiteMetricRepository extends JpaRepository <SiteMetric,Integer > {

    @Query(value = "SELECT COALESCE(SUM(s.site_metric_count),0)\n" +
            " FROM site_metric s", nativeQuery = true)
    Integer getTotalVisits();

    @Query(value = "SELECT s.*\n" +
            "FROM site_metric s\n" +
            "ORDER BY s.site_metric_id DESC LIMIT 1", nativeQuery = true)
    SiteMetric getLastRow();
}


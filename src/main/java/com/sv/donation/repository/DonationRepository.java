package com.sv.donation.repository;

import com.sv.donation.domain.Donation;
import com.sv.donation.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DonationRepository extends JpaRepository <Donation, Integer >{

    Donation findTopByUserOrderByDonationIdDesc(User user);

    @Query(value =
            "SELECT d.* \n" +
            "  FROM donation d\n" +
            "  WHERE d.user_user_id = ?1\n" +
            "  ORDER BY d.donation_id DESC LIMIT 1", nativeQuery = true)
    Donation findLastDonationByUser(Integer userId);
}


package com.sv.donation.repository;

import com.sv.donation.domain.Country;
import com.sv.donation.domain.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository <Department,Integer>{

    List<Department> findByCountry(Country country);
}


package com.sv.donation.repository;

import com.sv.donation.domain.DonationView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DonationViewRepository extends JpaRepository<DonationView, Integer> {
    List<DonationView> findAll();
}

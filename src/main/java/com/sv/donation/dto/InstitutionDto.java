package com.sv.donation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InstitutionDto {
    Integer institutionId;
    String institutionName;
}

package com.sv.donation.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class DonationFormDto {
    @NotNull
    Integer institutionId;
    @NotEmpty
    String amount;
}

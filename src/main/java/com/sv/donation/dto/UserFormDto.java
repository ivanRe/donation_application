package com.sv.donation.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UserFormDto {

    @NotEmpty(message = "person name can not be empty")
    String personName;
    @NotEmpty
    String personSureName;
    @NotEmpty
    @Email
    String personEmail;
    @NotEmpty
    String personDocumentId;
    @NotNull
    Integer personDepartmentId;
    @NotNull
    String userName;
    @NotEmpty
    String userPassword;
}

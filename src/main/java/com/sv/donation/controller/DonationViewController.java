package com.sv.donation.controller;

import com.google.gson.Gson;
import com.sv.donation.domain.DonationView;
import com.sv.donation.service.DonationViewService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;

@Controller
public class DonationViewController {

    private DonationViewService donationViewService;

    public DonationViewController(DonationViewService donationViewService) {
        this.donationViewService = donationViewService;
    }

    @GetMapping("/api/donationInfo/getAll")
    @ResponseBody
    public List<DonationView> getAll(){
        return this.donationViewService.getAll();
    }

    @GetMapping("/api/donationInfo/download")
    public ResponseEntity<InputStreamResource> download() {

        byte[] buf = new Gson().toJson(this.donationViewService.getAll()).getBytes();
        String fileName = "service"  + new Date().toInstant() + ".json";
        return ResponseEntity.ok()
                .header("Content-Disposition",
                        "attachment; filename=\"" + fileName + "\"")
                .contentLength(buf.length)
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(new ByteArrayInputStream(buf)));
    }

    @GetMapping("/api/donationInfo")
    @ResponseBody
    public ResponseEntity<InputStreamResource> getInfo(@RequestParam String token) {
        if (token.equals("fdfsdf3493094kgfk292323234hgjgjfbdbf"))
        {
            byte[] buf = new Gson().toJson(this.donationViewService.getAll()).getBytes();
            String fileName = "service"  + new Date().toInstant() + ".json";
            return ResponseEntity.ok()
                    .header("Content-Disposition",
                            "attachment; filename=\"" + fileName + "\"")
                    .contentLength(buf.length)
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(new InputStreamResource(new ByteArrayInputStream(buf)));
        }
        else
            return null;
    }
}

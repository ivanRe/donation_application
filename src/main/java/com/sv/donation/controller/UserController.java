package com.sv.donation.controller;

import com.sv.donation.dto.UserFormDto;
import com.sv.donation.service.CountryService;
import com.sv.donation.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@Slf4j
public class UserController {

    private CountryService countryService;
    private UserService userService;

    public UserController(CountryService countryService, UserService userService) {
        this.countryService = countryService;
        this.userService = userService;
    }

    @GetMapping("/user/register")
    public String userRegistryPage(Model model){
        model.addAttribute("countries",this.countryService.getAll());
        model.addAttribute("userFormDto", new UserFormDto());
        return "register";
    }

    @PostMapping("/user/saveUser")
    public String registerUser(@Valid @ModelAttribute UserFormDto userFormDto,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("success", false);
            return "redirect:/user/register";
        }
        this.userService.guardarDTO(userFormDto);
        log.info(userFormDto.toString());
        redirectAttributes.addFlashAttribute("success", true);
        return "redirect:/user/register";
    }

    @GetMapping("/login")
    public String login(){
        return "login";
    }
}

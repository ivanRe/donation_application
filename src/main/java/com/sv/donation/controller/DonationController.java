package com.sv.donation.controller;

import com.sv.donation.dto.DonationFormDto;
import com.sv.donation.dto.UserDetailsDto;
import com.sv.donation.service.CountryService;
import com.sv.donation.service.DonationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDateTime;

@Controller
@Slf4j
public class DonationController {

    private CountryService countryService;
    private DonationService donationService;

    public DonationController(CountryService countryService, DonationService donationService) {
        this.countryService = countryService;
        this.donationService = donationService;
    }

    @GetMapping("/donation/newDonation")
    public String donationForm(Model model) {
        model.addAttribute("donationFormDto", new DonationFormDto());
        model.addAttribute("countries", this.countryService.getAll());
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetailsDto user = ((UserDetailsDto) principal);
        return this.donationService.getLastDonation(user.getUser().getUserId())
                .map(donation -> {
                            if (donation.getCreateDateTime().getMonth().equals(LocalDateTime.now().getMonth()))
                                return "/donationError";
                            else
                                return "/donation/donationForm";
                        }
                )
                .orElse("/donation/donationForm");
    }

    @PostMapping("/donation/saveDonation")
    public String saveDonation(@Valid @ModelAttribute DonationFormDto donationFormDto,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes) throws ParseException {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("success", false);
            return "redirect:/donation/newDonation";
        }
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetailsDto user = ((UserDetailsDto) principal);

        NumberFormat format = NumberFormat.getCurrencyInstance();
        Number number = format.parse(donationFormDto.getAmount().replaceAll("\\s+", ""));
        donationFormDto.setAmount(number.toString());
        this.donationService.save(donationFormDto, user.getUser().getUserId());
        redirectAttributes.addFlashAttribute("success", true);
        return "redirect:/donation/newDonation";
    }

    @GetMapping("/donation/Main")
    public String donationMainForm(){
        return "/donation/donationMain";
    }



}

package com.sv.donation.controller;

import com.sv.donation.dto.DepartmentDto;
import com.sv.donation.service.DepartmentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class DepartmentController {

    private DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/department/getDepartments")
    @ResponseBody
    public List<DepartmentDto> getDepartmentsByCountry(@RequestParam Integer countryId){
        return this.departmentService.getAllByCountry(countryId).stream()
                .map(department -> new DepartmentDto(department.getDepartmentId(),department.getDepartmentName()))
                .collect(Collectors.toList());
    }
}

package com.sv.donation.controller;

import com.sv.donation.dto.CountryDto;
import com.sv.donation.service.CountryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;
/*TODO: DELETE*/
@Controller
public class CountryController {

    private CountryService countryService;


    public CountryController(CountryService countryService) {
        this.countryService = countryService;

    }

    @GetMapping("/country/getCountries")
    @ResponseBody
    public List<CountryDto> getCountries(){
        return this.countryService.getAll().stream()
                .map(country -> new CountryDto(country.getCountryId(), country.getCountryName()))
                .collect(Collectors.toList());
    }
}

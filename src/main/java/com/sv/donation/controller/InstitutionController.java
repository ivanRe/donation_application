package com.sv.donation.controller;

import com.sv.donation.dto.InstitutionDto;
import com.sv.donation.service.InstitutionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class InstitutionController {

    private InstitutionService institutionService;

    public InstitutionController(InstitutionService institutionService) {
        this.institutionService = institutionService;
    }

    @GetMapping("institution/getAllByCountry")
    @ResponseBody
    List<InstitutionDto> getAllByCountry(Integer countryId){
        return this.institutionService.getAllByCountry(countryId).stream()
                .map(institution -> new InstitutionDto(institution.getInstitutionId(), institution.getInstitutionName()))
                .collect(Collectors.toList());
    }
}

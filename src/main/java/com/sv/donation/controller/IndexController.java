package com.sv.donation.controller;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.io.*;


@Controller
public class IndexController {

    private com.sv.donation.configuration.MetricConfiguration MetricConfiguration;

    public IndexController(com.sv.donation.configuration.MetricConfiguration metricConfiguration) {
        MetricConfiguration = metricConfiguration;
    }

    @GetMapping("/index")
    public String showIndex(Model model){
        this.MetricConfiguration.increment();
        int count = (int) Math.round(this.MetricConfiguration.getVisitCount());
        model.addAttribute("visits_count", count);
        return "/index";
    }

    @GetMapping("/admin/Main")
    public String showAdminMain(Model model){
        return "/admin/adminMain";
    }

    @GetMapping("/admin/Download/visitorCount")
    public ResponseEntity<InputStreamResource> downloadVisitorCount() throws IOException {
        String filePath = System.getProperty("user.dir") + "/src/main/resources/static/Files/visit_counter.txt";
        File file = new File(filePath);
        InputStream targetStream = new FileInputStream(file);
        return ResponseEntity.ok()
                .header("Content-Disposition",
                        "attachment; filename=\"" + file.getName() + "\"")
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(targetStream));
    }

}

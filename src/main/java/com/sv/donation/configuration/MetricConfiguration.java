package com.sv.donation.configuration;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class MetricConfiguration {

    private Counter visit;

    public MetricConfiguration(MeterRegistry meterRegistry) {
        this.visit = meterRegistry.counter("visitors.entrance", "time","daily");
    }

    public void increment(){
        visit.increment();
    }

    public Double getVisitCount(){
        return this.visit.count();
    }
}

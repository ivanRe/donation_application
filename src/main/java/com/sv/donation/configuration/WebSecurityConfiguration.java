package com.sv.donation.configuration;

import com.sv.donation.service.CustomUserDetails;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@ConditionalOnProperty(value = "app.security.basic.enabled", havingValue = "true")
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private CustomUserDetails customUserDetails;
    private CustomSuccessHandler customSuccessHandler;


    public WebSecurityConfiguration(CustomUserDetails customUserDetails, CustomSuccessHandler customSuccessHandler) {
        this.customUserDetails = customUserDetails;
        this.customSuccessHandler = customSuccessHandler;
    }

    @Bean
    public BCryptPasswordEncoder BpasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/api/**").permitAll()
                .antMatchers("/admin/**").hasAuthority("ADMIN")
                .antMatchers("/donation/**").hasAuthority("USER")
                .and()
                .authorizeRequests()
                .antMatchers("/resources/static/**", "/user/**", "/index").permitAll()
                .antMatchers("/css/**", "/js/**", "/images/**").permitAll()
                .and()
                .authorizeRequests().antMatchers("/login", "/resource/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .successHandler(customSuccessHandler)
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login?logout")
                .clearAuthentication(true)
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .permitAll();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetails).passwordEncoder(BpasswordEncoder());
    }

    @Override
    public void configure(WebSecurity web) {
        web
                .ignoring()
                .antMatchers("/resources/**")
                .and()
                .ignoring()
                .antMatchers("/css/**", "/js/**", "/images/**")
                .and()
                .ignoring()
                .antMatchers("/institution/getAllByCountry", "/department/getDepartments", "/user/**","/actuator/**");

    }

}

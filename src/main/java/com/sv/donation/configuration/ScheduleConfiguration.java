package com.sv.donation.configuration;

import com.sv.donation.domain.SiteMetric;
import com.sv.donation.service.SiteMetricService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;


@Component
@EnableScheduling
public class ScheduleConfiguration {

    private Counter counter;
    private SiteMetricService siteMetricService;

    public ScheduleConfiguration(MeterRegistry meterRegistry, SiteMetricService siteMetricService) {
        this.siteMetricService = siteMetricService;
        counter = meterRegistry.get("visitors.entrance").counter();
    }

    @Scheduled(cron = "0 0 * * * *")
    public void sendStatistics() throws IOException {
        SiteMetric siteMetric = new SiteMetric();
        siteMetric.setSiteMetricCount((int) (counter.count()-this.siteMetricService.getTotalVisits()));
        siteMetric = this.siteMetricService.saveDayMetrics(siteMetric);
        String filePath = System.getProperty("user.dir") + "/src/main/resources/static/Files/visit_counter.txt";
        FileWriter writer = new FileWriter(filePath, true);
        writer.write(System.lineSeparator());
        writer.write(siteMetric.getCreateDateTime().toString() + "\t\t" + siteMetric.getSiteMetricCount());
        writer.close();
    }
}
